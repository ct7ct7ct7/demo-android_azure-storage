package com.anson.demo_azure_storage;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.io.File;
import java.io.FileOutputStream;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = (Button)findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    //在Cache內產生檔案
                    String fileContent = "現在時間="+System.currentTimeMillis();
                    File saveFile = new File(MainActivity.this.getCacheDir(),"Test.txt");
                    FileOutputStream outStream = new FileOutputStream(saveFile);
                    outStream.write(fileContent.getBytes());
                    outStream.close();

                    //上傳
                    FileUploadTask fileUploadTask = new FileUploadTask(MainActivity.this,saveFile);
                    fileUploadTask.execute();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }
}
