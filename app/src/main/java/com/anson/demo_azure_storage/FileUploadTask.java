package com.anson.demo_azure_storage;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;

import java.io.File;
import java.io.FileInputStream;

/**
 * Created by Anson on 2015/8/23.
 */
public class FileUploadTask extends AsyncTask<Void,Void,Boolean> {
    //容器名稱
    private static final String CONTAINER_NAME = "你的容器名稱";
    //連結字串
    private static final String storageConnectionString =
            "DefaultEndpointsProtocol=http" +
                    ";"+
                    "AccountName=你的儲存體帳戶名稱" +
                    ";"+
                    "AccountKey=你的金鑰";

    private ProgressDialog mDialog;
    private File mUploadFile;
    private Context mContext;

    public FileUploadTask(Context context,File uploadFile) {
        mContext = context;
        mUploadFile = uploadFile;
    }

    @Override
    protected void onPreExecute() {
        mDialog = new ProgressDialog(mContext);
        mDialog.setTitle("Tips");
        mDialog.setMessage("Uploading...");
        mDialog.setCancelable(false);
        mDialog.show();
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        boolean status;
        try {
            CloudStorageAccount storageAccount = CloudStorageAccount.parse(storageConnectionString);
            CloudBlobClient blobClient = storageAccount.createCloudBlobClient();
            CloudBlobContainer container = blobClient.getContainerReference(CONTAINER_NAME);
            container.createIfNotExists();

            String blobName = System.currentTimeMillis()+".txt";
            CloudBlockBlob blockBlob = container.getBlockBlobReference(blobName);
            blockBlob.upload(new FileInputStream(mUploadFile), mUploadFile.length());
            status = true;
        } catch (Exception e) {
            status = false;
        }
        return status;
    }

    @Override
    protected void onPostExecute(Boolean status) {
        if (status == true) {
            Toast.makeText(mContext,"檔案上傳成功", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(mContext,"檔案上傳失敗", Toast.LENGTH_SHORT).show();
        }
        mDialog.cancel();
        super.onPostExecute(status);
    }
}
